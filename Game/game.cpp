/*************************************************



				GAME APPLICATION



*************************************************/


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using namespace cv;
using namespace std;

#define Q_C 113	// q key on keyboard
#define P_C 112 // p key 
#define A_C 97	// a key
#define O_C 111	// o key
#define L_C 108	// l key
#define S_C 115	// s key
#define N_C 110 // n key
#define Y_C 121 // y key
#define ESC 27	// esc key
#define TILE_WIDTH 20	// width of tile
#define TILE_HEIGHT 110	// height of tile
#define FINGER_WIDTH 80	// width of finger detection field
#define	IMG_WIDTH 1024 // image width
#define	IMG_HEIGHT 700	// image height
#define BALL_RADIUS 14	// ball radius

int dir_change(Point pt, int direction, Point p1_start, Point p1_end, Point p2_start, Point p2_end);
void printScreen(Mat image, Point p1_start, Point p1_end, Point p2_start, Point p2_end, Point ball_center, bool p1_moved, bool p2_moved);
void initializeScreen(Mat image, Point p1_start, Point p1_end, Point p2_start, Point p2_end, Point ball_center);
void showScore(Mat image);

//			             0   1   2   3   4    5    6    7   8   9  10  11  12  13  14  15
const int x_dir[] = {1,  1,  1,  1,  0,  -1,  -1,  -1, -1, -1, -1, -1,  0,  1,  1,  1}; // ball movement on x axis
const int y_dir[] = {0, -1, -1, -1, -1,  -1,  -1,  -1,  0,  1,  1,  1,  1,  1,  1,  1}; // ball movement on y axis
//int cnt = 0;
int reset = 0;	
int hit = 0;
int score[] = {0, 0};
int speed = 5;
const int lineType = 8;   
const int img_half_width = IMG_WIDTH/2;
const int img_half_height = IMG_HEIGHT/2;
const int tile_half_height = TILE_HEIGHT/2;
const int tile_third = TILE_HEIGHT/3;
const Scalar white = 255;//Scalar(255, 255, 255); // RBG code for white colour
const Scalar black = 0;//Scalar(0, 0, 0);		// RGB code for black colour
const Scalar purple = 100;//Scalar(123, 65, 100);	// RGB code for purple colour

// function that calculates if direction change is needed
int dir_change(Point pt, int direction, Point p1_start, Point p1_end, Point p2_start, Point p2_end){
	
	int dir = direction;
	int ball_right_edge = pt.x+BALL_RADIUS;
	int ball_left_edge = pt.x-BALL_RADIUS;
	int ball_uper_edge = pt.y-BALL_RADIUS;
	int ball_lover_edge = pt.y+BALL_RADIUS;
	
	int p1_tile_edge = FINGER_WIDTH+TILE_WIDTH;
	int p2_tile_edge = IMG_WIDTH-FINGER_WIDTH-TILE_WIDTH;
	int p1_tile_middle_up = p1_start.y+tile_third;
	int p1_tile_middle_down = p1_tile_middle_up+tile_third;
	int p2_tile_middle_up = p2_start.y+tile_third;
	int p2_tile_middle_down = p2_tile_middle_up+tile_third;
	
	if( (ball_right_edge >= p2_tile_edge) &&
	    (pt.y >= p2_start.y) && (pt.y <= p2_end.y) ){
	    
	    hit = 2;
	    speed += 1;
	
		if(pt.y < p2_tile_middle_up){
			if(direction == 3)
				return 5;
			else if(direction > 12)
				return (23 - direction);
			else
				return (7 - direction);
		}
		
		else if(pt.y > p2_tile_middle_down){
			if(direction == 13)
				return 11;
			else if(direction < 4)
				return (9 - direction);
			else
				return (25 - direction);
		}
		
		else{
			if(direction <= 8)
				return (8 - direction);
			else
				return (24 - direction);  	 
		} 
	}
	
	else if( (ball_left_edge <= p1_tile_edge) &&
			 (pt.y >= p1_start.y) && (pt.y <= p1_end.y) ){
			 
		hit = 1;
		speed += 1;
		
		if(pt.y < p1_tile_middle_up){
			if(direction == 5)
				return 3;
			else if(direction > 9)
				return (25 - direction);
			else
				return (9 - direction);
		}
		
		else if(pt.y > p1_tile_middle_down){
			if(direction == 11)
				return 11;
			else if(direction > 7)
				return (23 - direction);
			else
				return (7 - direction);
		}
		
		else{
			if(direction <= 8)
				return (8 - direction);
			else
				return (24 - direction); 
		} 
	}
	
	else if((ball_uper_edge <= 0) ||
	   		(ball_lover_edge >= IMG_HEIGHT)){
	   	
		return (16 - direction);  
		
	}
	
	else if((ball_right_edge > p2_tile_edge) ||
	   		(ball_left_edge < p1_tile_edge)){
	   
	    if(ball_left_edge <= FINGER_WIDTH){
	    	score[1] += 1;
	    	reset = 1;
	    	return dir;
	    }
	    else if(ball_right_edge >= IMG_WIDTH-FINGER_WIDTH){
	    	score[0] += 1;
	        reset = 1;
	        return dir;
	    }
	    
	    else if((ball_lover_edge == p1_start.y) ||
	   	   		(ball_uper_edge == p1_end.y)){
	   	   
	   		return (16 - direction);  
		}
		
		else if((ball_lover_edge == p2_start.y) ||
	   	   (ball_uper_edge == p2_end.y)){
	   	   
	   		return (16 - direction); 
		}
	 
	}
	
	return dir;
}

// function that initialize and restart the screen
void initializeScreen(Mat image, Point p1_start, Point p1_end, Point p2_start, Point p2_end, Point ball_center){

	rectangle(image, Point(FINGER_WIDTH+TILE_WIDTH,0), Point(IMG_WIDTH-FINGER_WIDTH-TILE_WIDTH, IMG_HEIGHT), black, CV_FILLED, lineType, 0);
	circle(image, ball_center, BALL_RADIUS, white, CV_FILLED, lineType, 0); // ball
	rectangle(image, Point(FINGER_WIDTH, 0), Point(FINGER_WIDTH+TILE_WIDTH, IMG_HEIGHT), black, CV_FILLED, lineType, 0);
	rectangle(image, Point(IMG_WIDTH-FINGER_WIDTH, 0), Point(IMG_WIDTH-FINGER_WIDTH-TILE_WIDTH, IMG_HEIGHT), black, CV_FILLED, lineType, 0);
	rectangle(image, p1_start, p1_end, white, CV_FILLED, lineType, 0); // p1 tile
	rectangle(image, p2_start, p2_end, white, CV_FILLED, lineType, 0); // p2 tile
	rectangle(image, Point(0,0), Point(FINGER_WIDTH-1, IMG_HEIGHT), purple, CV_FILLED, lineType, 0); // p1 finger
	rectangle(image, Point(IMG_WIDTH-FINGER_WIDTH+1,0), Point(IMG_WIDTH, IMG_HEIGHT), purple, CV_FILLED, lineType, 0); // p2 finger
	
	imshow("Display window", image);
	waitKey(1);
}

// function that prints the tiles, ball and score
void printScreen(Mat image, Point p1_start, Point p1_end, Point p2_start, Point p2_end, Point ball_center, bool p1_moved, bool p2_moved){
	
	// draw the ball
	circle(image, ball_center, BALL_RADIUS, white, CV_FILLED, lineType, 0);
	
	// draw the tile which is moved
	if((p1_moved == true) || (hit == 1)){
		p1_moved = false;
		rectangle(image, p1_start, p1_end, white, CV_FILLED, lineType, 0);  
		rectangle(image, Point(0,0), Point(FINGER_WIDTH-1, IMG_HEIGHT), purple, CV_FILLED, lineType, 0);
		hit = 0;
	} 
	if((p2_moved == true) || (hit = 2)){
		p2_moved = false;
		rectangle(image, p2_start, p2_end, white, CV_FILLED, lineType, 0);
		rectangle(image, Point(IMG_WIDTH-FINGER_WIDTH+1,0), Point(IMG_WIDTH, IMG_HEIGHT), purple, CV_FILLED, lineType, 0);
		hit = 0;
	}
		  
	// if somebody scored, show the score   
	if(reset)
		showScore(image);
}

// function for drawing score
void showScore(Mat image){

	char sc[] = "0  :  0";
	sprintf(sc, "%d  :  %d", score[0], score[1]);
	putText(image, sc, Point(228, img_half_height), FONT_HERSHEY_PLAIN, 10, white, 12, lineType);
}

int main(int argc, char ** argv){

  if(argc > 1){
    speed = atoi(argv[1]);
  }
  
	Mat image;
	int thickness = 2; 
  int direction = 0;

  char coordinates[10];
  int key_pressed = -1;
	int bytes_read = 0;
	
	char *p1;
	char *p2;
	
	// coordinates for player tiles
	int p1_y_old = 0;
	int p1_y_new = img_half_height;
	int p2_y_old = 0;
	int p2_y_new = img_half_height;
	bool p1_moved = false;
	bool p2_moved = false;
    
  // open fifo for receiving data from finger detection app
  char coordinates_fifo[] = "/tmp/coordinatesfifo";
  int coordinates_fd = open(coordinates_fifo, O_RDONLY | O_NONBLOCK);
    
	// read image
    image = imread("field.png", CV_LOAD_IMAGE_GRAYSCALE);
	if(!image.data){
		cout<<"Could not open or find the image"<<endl;
		return -1;	
	}
	
	namedWindow("Display window", WINDOW_AUTOSIZE);
	
	while(1){
		// wait for 's' command to start game
		cout << "GAME" << endl;
		bytes_read = read(coordinates_fd, coordinates, 3); // read the data from buffer
		while(coordinates[0] != 's'){
			bytes_read = read(coordinates_fd, coordinates, 3);
		}
		
		// coordinates for player 1 and 2 tiles and ball
		Point p1_start = Point(FINGER_WIDTH, p1_y_new-tile_half_height);
		Point p1_end = Point(FINGER_WIDTH+TILE_WIDTH, p1_y_new+tile_half_height);;
		Point p2_start = Point(IMG_WIDTH-FINGER_WIDTH, p2_y_new-tile_half_height);
		Point p2_end = Point(IMG_WIDTH-FINGER_WIDTH-TILE_WIDTH, p2_y_new+tile_half_height);
		Point ball_center = Point(img_half_width, img_half_height);

		initializeScreen(image, p1_start, p1_end, p2_start, p2_end, ball_center);

		while(1){

			if(reset){	// reset ocures when a player scores
				ball_center = Point(img_half_width, img_half_height);
				reset++;
				speed = atoi(argv[1]);
				direction = std::rand() % 16;
        if((direction == 4) || (direction == 12))
          direction ++;
          
				if(reset == 3){
					reset = 0;
					initializeScreen(image, p1_start, p1_end, p2_start, p2_end, ball_center);
				}
				sleep(1);
			}	
			else{				 	   	
				// erase the ball (draw a black one over it)
				circle(image, ball_center, BALL_RADIUS, black, CV_FILLED, lineType, 0);
	
				// calculate the ball direction
				direction = dir_change(ball_center, direction, p1_start, p1_end, p2_start, p2_end);
	
				// calculate the new ball coordinates
				if((direction == 1) || (direction == 7) || (direction == 9) || (direction == 15)){
					ball_center.x += 2*x_dir[direction]*speed;
					ball_center.y += y_dir[direction]*speed;
				}
				else if((direction == 3) || (direction == 5) || (direction == 11) || (direction == 13)){
					ball_center.y += 2*y_dir[direction]*speed;
					ball_center.x += x_dir[direction]*speed;
				}
				else{
					ball_center.x += 2*x_dir[direction]*speed;
					ball_center.y += 2*y_dir[direction]*speed;
				}

				// calculate the new possition of tile if the tile is moved
				if(p1_moved){
					rectangle(image, p1_start, p1_end, black, CV_FILLED, lineType, 0);
					p1_y_old = p1_y_new;
					p1_start.y = p1_y_new-tile_half_height;
					p1_end.y = p1_y_new+tile_half_height;
				}
				if(p2_moved){
					rectangle(image, p2_start, p2_end, black, CV_FILLED, lineType, 0);
					p2_y_old = p2_y_new;
					p2_start.y = p2_y_new-tile_half_height;
					p2_end.y = p2_y_new+tile_half_height;
				}		

				printScreen(image, p1_start, p1_end, p2_start, p2_end, ball_center, p1_moved, p2_moved);

				imshow("Display window", image);

				key_pressed = waitKey(1); // waitKey function returns an integer that represents pushed key
	
				// if the S, P or ESC are pressed do the folowing
				switch(key_pressed){

					case S_C:
						speed ++; break;
		
					case P_C:
						putText(image, "P A U S E", Point(350, 50), FONT_HERSHEY_PLAIN, 4, white, 8, lineType);
						imshow("Display window", image);
						key_pressed = waitKey(0);
						while(key_pressed != P_C){	
							key_pressed = waitKey(0);
						} 
						putText(image, "P A U S E", Point(350, 50), FONT_HERSHEY_PLAIN, 4, black, 8, lineType);
						break;
			
					case ESC: // exit the game
						close(coordinates_fd);
						return 0;		
				}
	
				bytes_read = read(coordinates_fd, coordinates, 10); // read the coordinates in format p1_coor:p2_coor
				//cout << coordinates << endl;
				if(bytes_read != -1){
					p1 = strtok(coordinates, ":");
					p2 = strtok(NULL, ":");
					p1_y_new = atoi(p1)*7; // *7 because the finger detection application sends values 0-100 but we have the playing field the size of 700 pixels
					p2_y_new = atoi(p2)*7;

					if(p1_y_new != p1_y_old)
						p1_moved = true;
					if(p1_y_new != p1_y_old)
						p2_moved = true;
				}
			}
		}
	}

	return 0;
}
