# **Computer vision with Zybo** #

**The idea** of this project is to emulate touch screen using regular TFT monitor and USB Web camera. Basically, if you point a camera to capture the image on the screen, with the detection software you can track a moving object, in this case a finger, extract its coordinates and pass it to a desired application. The application, in this case a game, use this coordinates to move objects and thus emulate touch screen. The next picture represents this idea. Also the idea is to enable two controllers to play the game.

**The chosen hardware** for this project is the Zybo ZynqTM-7000 Development Board from Xilinx. The ZYBO (Zynq Board) is a feature-rich, ready-to-use, entry-level embedded software and digital circuit development platform built around the smallest member of the Xilinx Zynq-7000 family, the Z-7010. The Z-7010 is based on the Xilinx All Programmable System-on-Chip (AP SoC) architecture, which tightly integrates a dual-core ARM Cortex-A9 processor with Xilinx 7-series Field Programmable Gate Array (FPGA) logic.

![Digilent contest.jpg](https://bitbucket.org/repo/ddkaaX/images/1491738509-Digilent%20contest.jpg)

[VIDEO](https://www.youtube.com/watch?v=YOsZf0xflkw)