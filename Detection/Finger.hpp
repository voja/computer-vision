#pragma once
#include <string>
#include "opencv2/highgui/highgui.hpp"
using namespace std;

class Finger
{
	public:
		Finger(void);
		~Finger(void);
		
		Finger(string name);
		
		int getXPos();
		void setXPos(int x);
		
		int getYPos();
		void setYPos(int y);
		
		cv::Scalar getHSVmin();
		cv::Scalar getHSVmax();
		
		void setHSVmin(cv::Scalar min);
		void setHSVmax(cv::Scalar max);
		
		string getType(){
			return type;
		}
		void setType(string t){
			type = t;
		}
		
	private:
		int xPos, yPos;
		string type;
		cv::Scalar HSVmin, HSVmax;
};
