/************************************************************************


					Finger detection application


************************************************************************/
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include "Finger.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdio.h>
#include <cv.h>
#include <highgui.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>

using namespace std;
using namespace cv;

typedef enum{
	IDLE,
	LOAD,
	SNAPSHOT,
	FINGER,
	TRACKING
} fsm_state; 

//initial min and max HSV filter values.
int H_MIN = 0;
int H_MAX = 256;
int S_MIN = 0;
int S_MAX = 256;
int V_MIN = 0;
int V_MAX = 256;
fsm_state state = IDLE; // initial state
const int FRAME_WIDTH = 640; //default capture width and height
const int FRAME_HEIGHT = 480;
const int MAX_NUM_POLIGONS=2; //max number of poligons for fingers to be detected in frame
const int MAX_NUM_OBJECTS=3; //max number of objects to be detected in frame
const int MIN_OBJECT_AREA = 10*10; //minimum and maximum object area
const int MAX_OBJECT_AREA = FRAME_HEIGHT*FRAME_WIDTH/1.5;
const string windowName = "Original Image"; //names that will appear at the top of each window
const string windowName1 = "HSV Image";
const string windowName2 = "Thresholded Image";
const string windowName3 = "After Morphological Operations";
const string trackbarWindowName = "Trackbars";
int playerFingerY[MAX_NUM_OBJECTS];
int poligonMaxValue[MAX_NUM_POLIGONS];
int poligonMinValue[MAX_NUM_POLIGONS];
int player_position[2] = {50, 50}; // players coordinates

string intToString(int number){
	std::stringstream ss;
	ss << number;
	return ss.str();
}

// function that extraxts players Y coordinates
void drawObject(vector<Finger> theFingers,Mat &frame){
	
	for(unsigned int i = 0; i<theFingers.size(); i++){
		playerFingerY[i] = theFingers.at(i).getYPos();
		
		//player 1 - left
		if(theFingers.at(i).getXPos() < (FRAME_WIDTH/2)){
			player_position[0] = ((double)(playerFingerY[i] - poligonMinValue[0])/(double)(poligonMaxValue[0] - poligonMinValue[0]))*100;//scalePos;
			cout<<"Finger Player1 left "<<" Y: "<<player_position[0]<<endl;
		}else{
			player_position[1] = ((double)(playerFingerY[i] - poligonMinValue[1])/(double)(poligonMaxValue[1] - poligonMinValue[1]))*100;//scalePos;
			cout<<"Finger Player2 right "<<" Y: "<<player_position[1]<<endl;
		}
	}
}

// function dilates and erodes the picture so that it would be smoother
void morphOps(Mat &thresh){

	//create structuring element that will be used to "dilate" and "erode" image.
	//the element chosen here is a 3px by 3px rectangle
	Mat erodeElement = getStructuringElement( MORPH_RECT,Size(3,3));
	
	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement( MORPH_RECT,Size(8,8));

	erode(thresh,thresh,erodeElement);
	dilate(thresh,thresh,dilateElement);
}

// function for tracking players fingers and determines the coordinates of them
void trackFilteredObject(Mat threshold){

	vector<Finger> fingers;

	//these two vectors needed for output of findContours
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	Moments moment;
	double area;
	Finger finger;
	
	//find contours of filtered image using openCV findContours function
	findContours(threshold,contours,hierarchy,CV_RETR_CCOMP,CV_CHAIN_APPROX_SIMPLE );
	
	//use moments method to find our filtered object
	bool objectFound = false;
	if (hierarchy.size() > 0) {
		int numObjects = hierarchy.size();
		//if number of objects greater than MAX_NUM_OBJECTS we have a noisy filter
		
		if(numObjects<MAX_NUM_OBJECTS){
			for (int index = 0; index >= 0; index = hierarchy[index][0]) {
				moment = moments((cv::Mat)contours[index]);
				area = moment.m00;

				//if the area is less than 10px by 10px then it is probably just noise
				//if the area is the same as the 3/2 of the image size, probably just a bad filter
				//we only want the object with the largest area so we safe a reference area each
				//iteration and compare it to the area in the next iteration.
				if(area>MIN_OBJECT_AREA){
					finger.setXPos(moment.m10/area);
					finger.setYPos(moment.m01/area);
						
					fingers.push_back(finger);
					objectFound = true;

				}
				else 
					objectFound = false;

			}

				drawObject(fingers,threshold); // extract the coordinates
		}
	}
}

int main(int argc, char* argv[])
{
	bool calibrationMode;
	int bytes_read = 0;
	//struct timeval start, end; // used for measuring time
	//long mtime, seconds, useconds;

  // continue program with calibration or to read values from file
	if(argc == 2){
		bytes_read = atoi(argv[1]);
		if(bytes_read == 0)
			calibrationMode = false;
		else
			calibrationMode = true;
	}
	else{
		cout << "Do you want to calibrate? 0/1" << endl;
		return 0;
	}
	
	bool pause = true;
	bool bSuccess;
	
	FILE *my_stream;
	FILE *push_btn0_fd, *push_btn1_fd, *push_btn2_fd, *push_btn3_fd; // file descriptors for push buttons
	
	IplImage* img;
	IplImage* imgGrayScaleOpen;
	IplImage* imgGrayScale;
	CvMemStorage *storage;

	vector<int> compression_params; //vector that stores the compression parameters of the image
	
	char key;
	int stop = 0;
	int contoursNum = 0;
	int close_error;
	int mode = 0;
	char *my_string;
	char push_btn0[] = "/sys/class/gpio/gpio73/value";
	char push_btn1[] = "/sys/class/gpio/gpio74/value";
	char push_btn2[] = "/sys/class/gpio/gpio75/value";
	char push_btn3[] = "/sys/class/gpio/gpio76/value";
	char send_buf[10];
	size_t nbytes = 100;
	int btn0, btn1, btn2, btn3;
	my_string = (char *) malloc (nbytes + 1);
	
	// open fifo for sending data to finger detection app
  char coordinates_fifo[] = "/tmp/coordinatesfifo";
  mkfifo(coordinates_fifo, 0666);
  int coordinates_fd = open(coordinates_fifo, O_WRONLY );//| O_NONBLOCK);
  char coordinates[10]; // buffer for writing the coordinates in format p1_coor:p2_coor
	
	//Matrix to store each frame of the webcam feed
	Mat frame1;
	Mat cameraFeed;
	Mat threshold;
	Mat thresholdPoli;
	Mat HSV;
	Mat res;
	
	//video capture object to acquire webcam feed
	VideoCapture capture;
	//open capture object at location zero (default location for webcam)
	capture.open(0);
	if(!capture.isOpened()){
		cout<<"ERROR ACQUIRING VIDEO FEED\n";
		getchar();
		return -1;
	}
	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH,FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT,FRAME_HEIGHT);
	
	while(!stop){
	
		switch(state){
		  case IDLE:
		    // in IDLE state the program waits on button btn0 to be pressed, and then takes a snapshot
				btn0 = 0;
				cout << "IDLE" << endl;
				if(calibrationMode == true){
					cout<<"Press btn0 to take a snapshoot"<<endl;
					while (pause == true){
						capture.read(frame1); // read one frame from camera
						key = waitKey(10);
						push_btn0_fd = fopen(push_btn0, "r"); // open the file for btn0
						bytes_read = fscanf(push_btn0_fd, "%d", &btn0); // read the btn0 value
						fclose(push_btn0_fd); // close the btn0 file
						if(btn0 == 1){
							btn0 = 0;
							pause = false;
						}
					}

					state = SNAPSHOT;
				}
				else
					state = LOAD; // if we dont want to calibrate, jump to LOAD state
				
				break;
		
			case SNAPSHOT:
			  // in the SNAPSHOT state, we change the values of HSV to extract just the rectangles where the fingers need to be placed
				cout << "SNAPSHOT" << endl;
				//show the original image
				namedWindow("Snapshoot image", CV_WINDOW_AUTOSIZE);
				imshow("Snapshoot image", frame1); // read the picture captured in IDLE state
		    cvtColor(frame1,HSV,COLOR_BGR2HSV); // change the colour mode of frame1 from BGR to HSV and save it to HSV
		
				cout<<"press btn0 and btn1 to exit calibration"<<endl;
		
				btn0 = 0;
				btn1 = 0;
				
				// btn0 - switch between H_MIN, H_MAX, S_MIN, S_MAX, V_MIN, V_MAX in right direction
				// btn1 - switch between H_MIN, H_MAX, S_MIN, S_MAX, V_MIN, V_MAX in left direction
				// btn2 - value +
				// btn3 - value -
				// btn0 + btn1 - exit the calibration mode
				while((btn0 == 0) || (btn1 == 0)){
					push_btn2_fd = fopen(push_btn2, "r");
					push_btn3_fd = fopen(push_btn3, "r");
					bytes_read = fscanf(push_btn2_fd, "%d", &btn2);
					bytes_read = fscanf(push_btn3_fd, "%d", &btn3);
					switch(mode){
						case 0:
							if(btn2 == 1){
								if(H_MIN < 254)
									H_MIN += 2;
								cout << "H_MIN = " << H_MIN << endl;
							}
							else if(btn3 == 1){
								if(H_MIN > 1)
									H_MIN -= 2;
								cout << "H_MIN = " << H_MIN << endl;	
							} break;
					
						case 1:
							if(btn2 == 1){
								if(H_MAX < 254)
									H_MAX++;
								cout << "H_MAX = " << H_MAX << endl;
							}
							else if(btn3 == 1){
								if(H_MAX > 1)
									H_MAX--;
								cout << "H_MAX = " << H_MAX << endl;	
							} break;
					
						case 2:
							if(btn2 == 1){
								if(S_MIN < 254)
									S_MIN += 2;
								cout << "S_MIN = " << S_MIN << endl;
							}
							else if(btn3 == 1){
								if(S_MIN > 1)
									S_MIN -= 2;
								cout << "S_MIN = " << S_MIN << endl;	
							} break;
					
						case 3:
							if(btn2 == 1){
								if(S_MAX < 254)
									S_MAX++;
								cout << "S_MAX = " << S_MAX << endl;
							}
							else if(btn3 == 1){
								if(S_MAX > 1)
									S_MAX--;
								cout << "S_MAX = " << S_MAX << endl;	
							} break;
					
						case 4:
							if(btn2 == 1){
								if(V_MIN < 254)
									V_MIN += 2;
								cout << "V_MIN = " << V_MIN << endl;
							}
							else if(btn3 == 1){
								if(V_MIN > 1)
									V_MIN -= 2;
								cout << "V_MIN = " << V_MIN << endl;	
							} break;
					
						case 5:
							if(btn2 == 1){
								if(V_MAX < 254)
									V_MAX++;
								cout << "V_MAX = " << V_MAX << endl;
							}
							else if(btn3 == 1){
								if(V_MAX > 1)
									V_MAX--;
								cout << "V_MAX = " << V_MAX << endl;	
							} break;
					}
					push_btn0_fd = fopen(push_btn0, "r");
					bytes_read = fscanf(push_btn0_fd, "%d", &btn0);
					push_btn1_fd = fopen(push_btn1, "r");
					bytes_read = fscanf(push_btn1_fd, "%d", &btn1);
					if(btn0 == 1){
						mode++;
						mode %= 6;
						cout << "mode = " << mode << endl;
					}
					if(btn1 == 1){
						mode--;
						if(mode < 0)
							mode = 5;
						cout << "mode = " << mode << endl;
					}
			
					fclose(push_btn0_fd);	
					fclose(push_btn1_fd);
					fclose(push_btn2_fd);
					fclose(push_btn3_fd);
						
					key = waitKey(10);
					inRange(HSV,Scalar(H_MIN,S_MIN,V_MIN),Scalar(H_MAX,S_MAX,V_MAX),thresholdPoli); // set value of HSV
					morphOps(thresholdPoli); // erode and dilate the picture for smoothness
					imshow("Filter",thresholdPoli); // show picture on screen
				}
		
		    
				compression_params.push_back(CV_IMWRITE_JPEG_QUALITY); //specify the compression technique
				compression_params.push_back(98); //specify the compression quality
				bSuccess = imwrite("frame.jpg", thresholdPoli, compression_params); //write the image to file
				if ( !bSuccess ){
					cout << "ERROR : Failed to save the image" << endl;
				}
		
				bSuccess = imwrite("frame1.jpg", frame1, compression_params); //write the image to file
				if ( !bSuccess ){
					cout << "ERROR : Failed to save the image" << endl;
				}
				destroyWindow("Filtered image");
				destroyWindow("Snapshoot image");
		
				img =  cvLoadImage("frame1.jpg");
				imgGrayScaleOpen =  cvLoadImage("frame.jpg");
		
				imgGrayScale = cvCreateImage(cvGetSize(imgGrayScaleOpen), 8, 1); 
				cvCvtColor(imgGrayScaleOpen,imgGrayScale,CV_BGR2GRAY);
		
				//thresholding the grayscale image to get better results
				cvThreshold(imgGrayScale,imgGrayScale,128,255,CV_THRESH_BINARY);
		
				CvSeq* contours;  //hold the pointer to a contour in the memory block
				CvSeq* result;   //hold sequence of points of a contour
				storage = cvCreateMemStorage(0); //storage area for all contours
		
		    // the next segment extracts the coordinates for playing poligons
				//finding all contours in the image
				cvFindContours(imgGrayScale, storage, &contours, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
		
				//iterating through each contour
				while(contours)
				{
					if(contoursNum >= MAX_NUM_POLIGONS)
						break;
					//obtain a sequence of points of contour, pointed by the variable 'contour'
					result = cvApproxPoly(contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, cvContourPerimeter(contours)*0.02, 0);
					  
					//if there are 4 vertices in the contour(It should be a quadrilateral)
					if(result->total==4 )
					{
						//iterating through each point
						cout<<"Poligon "<<contoursNum<<": "<<endl;
						CvPoint *pt[4];
						for(int i=0;i<4;i++){
							pt[i] = (CvPoint*)cvGetSeqElem(result, i);
							cout<<"Point "<<i<<": "<<"x: "<<pt[i]->x<<"  y: "<<pt[i]->y<<endl;
						}
						//drawing lines around the quadrilateral
						cvLine(img, *pt[0], *pt[1], cvScalar(0,255,0),4);
						cvLine(img, *pt[1], *pt[2], cvScalar(0,255,0),4);
						cvLine(img, *pt[2], *pt[3], cvScalar(0,255,0),4);
						cvLine(img, *pt[3], *pt[0], cvScalar(0,255,0),4);
						//sort points of poligons
						int i, j;
						int array_size = 4;
						CvPoint *temp;

						for (i = 0; i < (array_size - 1); ++i){
							for (j = 0; j < array_size - 1 - i; ++j ){
								if ((pt[j]->y) > (pt[j+1]->y)){
									temp = pt[j+1];
									pt[j+1] = pt[j];
									pt[j] = temp;
								}
							}
						}
						//player1 - left poligon
						if((pt[0]->x) < (FRAME_WIDTH/2)){
							cout<<pt[0]->x<<" < "<< FRAME_WIDTH/2<<endl;
							poligonMaxValue[0] = (pt[3]->y + pt[2]->y)/2;
							poligonMinValue[0] = (pt[0]->y + pt[1]->y)/2;
							cout<<"poligonMaxValue "<<0<<" : "<<poligonMaxValue[0]<<endl;
							cout<<"poligonMinValue "<<0<<" : "<<poligonMinValue[0]<<endl;
						//player2 - right poligon
						}
						else{
							poligonMaxValue[1] = (pt[3]->y + pt[2]->y)/2;
							poligonMinValue[1] = (pt[0]->y + pt[1]->y)/2;
							cout<<"poligonMaxValue "<<1<<" : "<<poligonMaxValue[1]<<endl;
							cout<<"poligonMinValue "<<1<<" : "<<poligonMinValue[1]<<endl;
						}
					}
					//obtain the next contour
					contours = contours->h_next;
					contoursNum++;
				}
		
				cout<<"Are thoes your playing poligons? (y/n)"<<endl;
				
				//show the image in which identified shapes are marked   
				cvNamedWindow("Poligons", CV_WINDOW_AUTOSIZE);
				cvShowImage("Poligons",img);
				cvWaitKey(100);
				btn2 = 0;
				btn3 = 0;
				
				// btn2 - "yes, those are my playing poligons", advance to state FINGER
				// btn3 - "no, those are not my playing poligons", go back to SNAPSHOT
				while((btn2 == 0) && (btn3 == 0)){
					push_btn2_fd = fopen(push_btn2, "r");
					push_btn3_fd = fopen(push_btn3, "r");
					bytes_read = fscanf(push_btn2_fd, "%d", &btn2);
					bytes_read = fscanf(push_btn3_fd, "%d", &btn3);
					fclose(push_btn2_fd);
					fclose(push_btn3_fd);
				}
				if(btn2 == 1){
					state = FINGER;
					
					// write the values of poligons to calibration.txt
					my_stream = fopen("calibration.txt", "w");
					if(my_stream == NULL){
						cout<<"File could not be opened"<<endl;
					}else{
						cout<<"File opened!"<<endl;
						for(int i = 0; i< contoursNum; i++)
							fprintf(my_stream, "%d\n", poligonMaxValue[i]);
						for(int i = 0; i< contoursNum; i++)
							fprintf(my_stream, "%d\n", poligonMinValue[i]);
					}
					fclose(my_stream);
				}
				else if(btn3 == 1)
					state = SNAPSHOT;
		
				cvDestroyAllWindows(); 
				break;
		
			//if calibration mode is not on we have defoult valeous for colors
			case LOAD:
			  // in state LOAD the values of poligon coordinates and the values of HSV are loaded
			  
				cout << "LOAD" << endl;

				my_stream = fopen("calibration.txt", "r");
				if(my_stream == NULL){
					printf("\nFile could not be opened\n");
				}
				else{
					bytes_read = getline(&my_string, &nbytes, my_stream);
					poligonMaxValue[0] = atoi(my_string);
					bytes_read = getline(&my_string, &nbytes, my_stream);
					poligonMaxValue[1] = atoi(my_string);
					bytes_read = getline(&my_string, &nbytes, my_stream);
					poligonMinValue[0] = atoi(my_string);
					bytes_read = getline(&my_string, &nbytes, my_stream);
					poligonMinValue[1] = atoi(my_string);
					bytes_read = getline(&my_string, &nbytes, my_stream);
					H_MIN = atoi(my_string);
					bytes_read = getline(&my_string, &nbytes, my_stream);
					S_MIN = atoi(my_string);
					bytes_read = getline(&my_string, &nbytes, my_stream);
					V_MIN = atoi(my_string);
					bytes_read = getline(&my_string, &nbytes, my_stream);
					H_MAX = atoi(my_string);
					bytes_read = getline(&my_string, &nbytes, my_stream);
					S_MAX = atoi(my_string);
					bytes_read = getline(&my_string, &nbytes, my_stream);
					V_MAX = atoi(my_string);
				}
			
				close_error = fclose (my_stream);
				if (close_error != 0)
					printf ("File could not be closed.\n");
				
				state = TRACKING;
				break;
	
			case FINGER:
			  // in state FINGER, HSV values are changed to detect only fingers on screen
				cout << "FINGER" << endl;
				H_MIN = 0;
				H_MAX = 256;
				S_MIN = 0;
				S_MAX = 256;
				V_MIN = 0;
				V_MAX = 256;
	
				thresholdPoli = imread("frame.jpg", 0); // open the picture with frame
				cv::threshold(thresholdPoli, thresholdPoli, 100, 255, THRESH_BINARY); // create a mask threasholdPoli used to observe only the poligon areas
				mode = 0;
				btn0 = 0;
				btn1 = 0;
				btn2 = 0;
				btn3 = 0;
				
				// btn0 - switch between H_MIN, H_MAX, S_MIN, S_MAX, V_MIN, V_MAX in right direction
				// btn1 - switch between H_MIN, H_MAX, S_MIN, S_MAX, V_MIN, V_MAX in left direction
				// btn2 - value +
				// btn3 - value -
				// btn0 + btn1 - exit the calibration mode
				while((btn0 == 0) || (btn1 == 0)){
					push_btn2_fd = fopen(push_btn2, "r");
					push_btn3_fd = fopen(push_btn3, "r");
					bytes_read = fscanf(push_btn2_fd, "%d", &btn2);
					bytes_read = fscanf(push_btn3_fd, "%d", &btn3);
					switch(mode){
						case 0:
							if(btn2 == 1){
								if(H_MIN < 254)
									H_MIN += 2;
								cout << "H_MIN = " << H_MIN << endl;
							}
							else if(btn3 == 1){
								if(H_MIN > 1)
									H_MIN -= 2;
								cout << "H_MIN = " << H_MIN << endl;	
							} break;
			
						case 1:
							if(btn2 == 1){
								if(H_MAX < 254)
									H_MAX += 2;
								cout << "H_MAX = " << H_MAX << endl;
							}
							else if(btn3 == 1){
								if(H_MAX > 1)
									H_MAX -= 2;
								cout << "H_MAX = " << H_MAX << endl;	
							} break;
			
						case 2:
							if(btn2 == 1){
								if(S_MIN < 254)
									S_MIN += 2;
								cout << "S_MIN = " << S_MIN << endl;
							}
							else if(btn3 == 1){
								if(S_MIN > 1)
									S_MIN -= 2;
								cout << "S_MIN = " << S_MIN << endl;	
							} break;
			
						case 3:
							if(btn2 == 1){
								if(S_MAX < 254)
									S_MAX += 2;
								cout << "S_MAX = " << S_MAX << endl;
							}
							else if(btn3 == 1){
								if(S_MAX > 1)
									S_MAX -= 2;
								cout << "S_MAX = " << S_MAX << endl;	
							} break;
			
						case 4:
							if(btn2 == 1){
								if(V_MIN < 254)
									V_MIN += 2;
								cout << "V_MIN = " << V_MIN << endl;
							}
							else if(btn3 == 1){
								if(V_MIN > 1)
									V_MIN -= 2;
								cout << "V_MIN = " << V_MIN << endl;	
							} break;
			
						case 5:
							if(btn2 == 1){
								if(V_MAX < 254)
									V_MAX += 2;
								cout << "V_MAX = " << V_MAX << endl;
							}
							else if(btn3 == 1){
								if(V_MAX > 1)
									V_MAX -= 2;
								cout << "V_MAX = " << V_MAX << endl;	
							} break;
					}
					push_btn0_fd = fopen(push_btn0, "r");
					bytes_read = fscanf(push_btn0_fd, "%d", &btn0);
					push_btn1_fd = fopen(push_btn1, "r");
					bytes_read = fscanf(push_btn1_fd, "%d", &btn1);
					if(btn0 == 1){
						mode++;
						mode %= 6;
						cout << "mode = " << mode << endl;
					}
					if(btn1 == 1){
						mode--;
						if(mode < 0)
							mode = 5;
						cout << "mode = " << mode << endl;
					}
		
					fclose(push_btn0_fd);	
					fclose(push_btn1_fd);
					fclose(push_btn2_fd);
					fclose(push_btn3_fd);
		
					capture.read(cameraFeed); // capture the frame from camera
					cameraFeed.copyTo(res, thresholdPoli); // copy the frame to res and apply the mask thresholdPoli
					cvtColor(res,res,COLOR_BGR2HSV); // convert the image from RGB to HSV
					inRange(res,Scalar(H_MIN,S_MIN,V_MIN),Scalar(H_MAX,S_MAX,V_MAX),threshold); // change the HSV value
					morphOps(threshold); // erode and dilate the image
					imshow(windowName2,threshold);
					key = waitKey(1);
				}
				cvDestroyAllWindows(); 
			
			  // save the HSV values for fingers in calibration.txt
				my_stream = fopen("calibration.txt", "a");
				if(my_stream == NULL){
					cout<<"File could not be opened"<<endl;
				}
				else{
					cout<<"File opened!"<<endl;
					fprintf(my_stream, "%d\n", H_MIN);
					fprintf(my_stream, "%d\n", S_MIN);
					fprintf(my_stream, "%d\n", V_MIN);
					fprintf(my_stream, "%d\n", H_MAX);
					fprintf(my_stream, "%d\n", S_MAX);
					fprintf(my_stream, "%d\n", V_MAX);
				}
				fclose(my_stream);
				state = TRACKING;
				break;
	
		case TRACKING:
		  // in state TRACKING the coordinates of the figers are extracted and sent to game application trough buffer
		  
			cout << "TRACKING" << endl;
			thresholdPoli = imread("frame.jpg", 0);
			cv::threshold(thresholdPoli, thresholdPoli, 100, 255, THRESH_BINARY);
			bytes_read = write(coordinates_fd, "s", sizeof("s")); // send 's' caracter to game applicatio to initiate the start

			while(1){
				//gettimeofday(&start, NULL);
				sprintf(coordinates, "%d:%d", player_position[0], player_position[1]); // form a coordinates message
				bytes_read = write(coordinates_fd, coordinates, sizeof(coordinates)); // write the message to buffer
				capture.read(cameraFeed); // capture the frame from camera
				cameraFeed.copyTo(res, thresholdPoli); // copy the frame to res and apply the mask thresholdPoli
				cvtColor(res,res,COLOR_BGR2HSV); // convert the image from RGB to HSV
				inRange(res,Scalar(H_MIN,S_MIN,V_MIN),Scalar(H_MAX,S_MAX,V_MAX),threshold); // change the HSV value
				morphOps(threshold); // erode and dilate the image
				trackFilteredObject(threshold); // extract coordinates

				//gettimeofday(&end, NULL);
				//seconds = end.tv_sec - start.tv_sec;
				//useconds = end.tv_usec - start.tv_usec;
				//mtime = (seconds*1000 + useconds/1000) + 0.5;
				//cout << "Time: " << mtime << endl;
				key = waitKey(1); // press q to quit
				if(key == 'q'){
					break;
					stop = 1;
				}
			}
			state = TRACKING;
			break;
		}
	
	}
	
	close(coordinates_fd);
	unlink(coordinates_fifo);
	free(my_string);
	cvReleaseMemStorage(&storage);
	cvReleaseImage(&img);
	cvReleaseImage(&imgGrayScale);
	cvReleaseImage(&imgGrayScaleOpen);
	return 0;
}
