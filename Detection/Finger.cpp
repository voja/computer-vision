#include "Finger.hpp"

Finger::Finger(void)
{
	
}
Finger::Finger(string name){
	
	setType(name);
	
	if(name == "blueApple"){
		//test obejct
		setHSVmin(cv::Scalar(107,72,30));
		setHSVmax(cv::Scalar(138,184,119));
	}
}

Finger::~Finger(void)
{
	
}

int Finger::getXPos(){
	return Finger::xPos;
}

void Finger::setXPos(int x){
	Finger::xPos = x;
}

int Finger::getYPos(){
	return Finger::yPos;
}

void Finger::setYPos(int y){
	Finger::yPos = y;
}

cv::Scalar Finger::getHSVmax(){
	return Finger::HSVmax;
}

cv::Scalar Finger::getHSVmin(){
	return Finger::HSVmin;
}

void Finger::setHSVmax(cv::Scalar max){
	Finger::HSVmax = max;
}

void Finger::setHSVmin(cv::Scalar min){
	Finger::HSVmin = min;
}
